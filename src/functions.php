<?php

add_action('wp_enqueue_scripts', 'teatteri_enqueue_styles');

function teatteri_enqueue_styles() {
	$parent_style = 'twentysixteen-style';

	wp_enqueue_style($parent_style, get_template_directory_uri() . '/style.css');
	wp_enqueue_style('teatteri-style', get_stylesheet_directory_uri() . '/style.css', array($parent_style), wp_get_theme()->get('Version'));
}
