const path = require('path');
const fs = require('fs');
const util = require('util');
const chokidar = require('chokidar');
const glob = require('glob');
const yazl = require('yazl');
const debounce = require('debounce');
const mkdirp = require('mkdirp');
const postcss = require('postcss');
const autoprefixer = require('autoprefixer');

const ROOT = 'src';
const ZIP = 'dist/teema.zip';
const THEME = 'teatteri-teema';

const globAsync = util.promisify(glob);
const mkdirpAsync = util.promisify(mkdirp);
const readFileAsync = util.promisify(fs.readFile);

const watcher = chokidar.watch(ROOT);

watcher.once('ready', () => {
	const listener = debounce(buildZip, 1000);
	
	listener();

	watcher
		.on('add', listener)
		.on('change', listener)
		.on('unlink', listener)
		.on('addDir', listener)
		.on('unlinkDir', listener);
});

async function buildZip() {
	const files = await globAsync(`${ROOT}/**/*`, {
		nodir: true
	});

	await mkdirpAsync(path.dirname(ZIP));

	const zipfile = new yazl.ZipFile();

	files.forEach(async filePath => {
		if (path.extname(filePath) === '.css') {
			const css = await readFileAsync(filePath);

			const result = await postcss([autoprefixer])
				.process(css, { from: filePath });

			zipfile.addBuffer(Buffer.from(result.css), `${THEME}/${path.relative(ROOT, filePath)}`);
		} else {
			zipfile.addFile(filePath, `${THEME}/${path.relative(ROOT, filePath)}`);
		}
	});

	zipfile.outputStream.pipe(fs.createWriteStream(ZIP));
	zipfile.end();
}
